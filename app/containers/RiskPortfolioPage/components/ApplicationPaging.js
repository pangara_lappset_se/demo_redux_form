import React from 'react';
// Sy - Sub-Components

class ApplicationPaging extends React.Component {
    render() {
        return (<div className="application-paging">
            <span className="prev-btn">Previous</span>
            <ul className="paging">
                <li className="paging-item">
                    <div className="paging-btn active">1</div>
                </li>
                <li className="paging-item">
                    <div className="paging-btn">2</div>
                </li>
                <li className="paging-item">
                    <div className="paging-btn">3</div>
                </li>
            </ul>
            <span className="next-btn">Next</span>
        </div>);
    }
}

export default ApplicationPaging;
