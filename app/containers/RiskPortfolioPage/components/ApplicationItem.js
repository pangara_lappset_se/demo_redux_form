import React from 'react';
import PropTypes from 'prop-types';
// Sy - Sub-Components

class ApplicationItem extends React.Component {
    render() {
        const application = this.props.application;
        return (<li className="application-item">
            <div className="item-body">
                <a href="application" className="title">{application.name}</a>
                <div className="risk">
                    <div className="number red-text">{application.vul}</div>
                    <div className="desc">Vulnerabilities</div>
                </div>
                <div className="score">
                    <div className="number green-text">{application.score}</div>
                    <div className="desc">Score</div>
                </div>
                <div className="actions">
                    <span className="icon">
                        <i className="fa fa-pencil"></i>
                    </span>
                    <span className="icon">
                        <i className="fa fa-trash"></i>
                    </span>
                </div>
            </div>
        </li>);
    }
}

ApplicationItem.propTypes = {
    application: PropTypes.object.isRequired
};

export default ApplicationItem;
