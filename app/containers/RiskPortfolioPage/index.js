/*
 * RiskPortfolioPage
 *
 */
import React from 'react';
import {Helmet} from 'react-helmet';

// Sy - Sub-Components
import LeftSideBar from 'components/LeftSideBar';
import Breadcrumb from 'components/Breadcrumb';
import ApplicationItem from './components/ApplicationItem';
import ApplicationPaging from './components/ApplicationPaging';

export class RiskPortfolioPage extends React.PureComponent {

    renderApplications(applications) {
        if (applications.length > 0) {
            return applications.map((application) => (<ApplicationItem key={application.id} application={application}/>));
        }
        return [];
    }

    render() {
        // Mock application data
        const data = [
            {
                id: 1,
                name: 'Application A',
                vul: 206,
                score: 66
            }, {
                id: 2,
                name: 'Application B',
                vul: 206,
                score: 66
            }, {
                id: 3,
                name: 'Application C',
                vul: 206,
                score: 66
            }
        ];
        const applications = this.renderApplications(data);
        const menu = [
            {
                id: 1,
                name: 'Risk Portfolio'
            }
        ];
        return (<section className="main-wrapper">
            <Helmet>
                <title>Risk Portfolio Page</title>
                <meta name="description" content="This is Risk Portfolio page"/>
            </Helmet>
            <div>
                <LeftSideBar/>
                <div className="content-wrapper">
                    <Breadcrumb menu={menu}></Breadcrumb>
                    <div className="main-content">
                        <div className="page risk-page application-page">
                            <button type="button" className="btn btn-primary float-right">
                                <i className="fa fa-plus">&nbsp;&nbsp;Create Application</i>
                            </button>
                            <ul className="applications">
                                {applications}
                            </ul>
                            <ApplicationPaging></ApplicationPaging>
                        </div>
                    </div>
                </div>

            </div>
            {/* Sy - End: Page main content */}
        </section>);
    }
}

export default RiskPortfolioPage;
