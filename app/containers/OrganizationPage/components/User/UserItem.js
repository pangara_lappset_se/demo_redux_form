import React from 'react';
import PropTypes from 'prop-types';
import {Link, withRouter} from 'react-router-dom';

// Sy - Sub-Components

class UserItem extends React.Component {
    render() {
        const user = this.props.user;
        return (<tr>
            <td><Link to={'/my-organization/users/' + user.id}>{user.name}</Link></td>
            <td>{user.username}</td>
            <td>{user.email}</td>
        </tr>);
    }
}

UserItem.propTypes = {
    user: PropTypes.object.isRequired,
    match: PropTypes.object
};

export default withRouter(UserItem);
