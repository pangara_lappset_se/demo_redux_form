import React from 'react';

// Sy - Redux extensions
import PropTypes from 'prop-types';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import {connect} from 'react-redux';
import {compose} from 'redux';

// Sy -- Call actions and reducer in Comp
import reducer from '../../redux/reducer';
import saga from '../../redux/saga';
import {getUserData} from '../../redux/actions';

// Sy - Sub-Components
import PartialNotFound from 'components/PartialNotFound';
import UserItem from './UserItem';

class UserSection extends React.Component {
    componentDidMount() {
        console.log('get Users');
        this.props.getUserData();
    }

    renderUser(users) {
        if (users.length > 0) {
            return users.map((user) => (<UserItem key={user.id} user={user}/>));
        }
        return [];
    }

    render() {
        console.log('users', this.props.users);
        const users = this.props.users.data ? this.renderUser(this.props.users.data) : [];
        return (<div className="section user-section">
            <div className="section-title">Users</div>
            <div className="users">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Username</th>
                            <th scope="col">Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        {users}
                    </tbody>
                </table>
            </div>
        </div>);
    }
}

UserSection.propTypes = {
    getUserData: PropTypes.func.isRequired,
    users: PropTypes.object.isRequired
};

function mapDispatchToProps(dispatch) {
    return {
        getUserData: () => {
            dispatch(getUserData());
        }
    };
}

function mapStateToProps(state) {
    const users = state.get('users');
    return {users};
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({key: 'users', reducer});
const withSaga = injectSaga({key: 'users', saga});

export default compose(withReducer, withSaga, withConnect)(UserSection);
