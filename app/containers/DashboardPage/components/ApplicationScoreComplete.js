import React from 'react';
import PropTypes from 'prop-types';
// Sy - Sub-Components

class ApplicationScoreComplete extends React.Component {
	
    render() {
    	
    	console.log("Testing", this.props);
    	const scoreData = this.props.scoreData;
        
        return (<div className="row">
        			<div className="col">
        				<div className="summary">Score Completeness&nbsp;
        					<span className="blue-text font-weight-bold">100% </span>
        					(<span className="yellow-text font-weight-bold">3 </span>out of <span className="yellow-text font-weight-bold">3 </span>tools generate report)
    					</div>
    				</div>
				</div>);
    }
}

export default ApplicationScoreComplete;
