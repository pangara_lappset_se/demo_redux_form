import React from 'react';
import PropTypes from 'prop-types';
// Sy - Sub-Components

var StackChart = require("react-chartjs-2").HorizontalBar;

class ApplicationIssueCategory extends React.Component {

	 render() {
		const stackBarChart = this.props.stackBarChart;

		console.log('CHART DATA', stackBarChart);

	    return <StackChart data={stackBarChart.data} options={stackBarChart.options} width="600" height="250"/>
	 }
}
export default ApplicationIssueCategory;
