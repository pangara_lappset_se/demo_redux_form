import React from 'react';
import PropTypes from 'prop-types';
// Sy - Sub-Components

class ApplicationScore extends React.Component {
	
    render() {
    	
    	console.log("Testing", this.props);
    	const scoreData = this.props.scoreData;
        
        return (<div className="row">
		        <div className="col">
		          <div className="score-item">
		            <div className="text">Coverity</div>
		            <div className="value">
		              <div className="number green-text green-br">{scoreData.coverity.score}</div>
		            </div>
		          </div>
		          <div className="pt-2"><i className="far fa-file-pdf h5">&nbsp;</i><span className="text-primary">SAST</span></div>
		          <div className="time">Last run: {scoreData.coverity.last_run}</div>
		        </div>
		        <div className="col">
		          <div className="score-item">
		            <div className="text">Protocode</div>
		            <div className="value">
		              <div className="number blue-text blue-br">{scoreData.protocode.score}</div>
		            </div>
		          </div>
		          <div className="pt-2"><i className="far fa-file-pdf h5">&nbsp;</i><span className="text-primary">SCA</span></div>
		          <div className="time">Last run: {scoreData.protocode.last_run}</div>
		        </div>
		        <div className="col">
		          <div className="score-item">
		            <div className="text">MS Portal</div>
		            <div className="value">
		              <div className="number yellow-text yellow-br">{scoreData.ms_portal.score}</div>
		            </div>
		          </div>
		          <div className="pt-2"><i className="far fa-file-pdf h5">&nbsp;</i><span className="text-primary">Managed Services Portal</span></div>
		          <div className="time">Last run: {scoreData.ms_portal.last_run}</div>
		        </div>
		      </div>);
    }
}

/*ApplicationScore.propTypes = {
	scoreData: PropTypes.object.isRequired
};*/

export default ApplicationScore;
