import React from 'react';
import PropTypes from 'prop-types';
// Sy - Sub-Components

class DownloadReport extends React.Component {
	
    render() {
        
        return (<div id="downloadReportModal" className="modal fade" role="dialog">
		        	<div className="modal-dialog">
		        		<div className="modal-content">
		        			<div className="modal-header">
		        				<h3 className="modal-title">Download Reports</h3>
		        			</div>
		        			<div className="modal-body">
		        				<p>Main content</p>
		        			</div>
		        			<div className="modal-footer">
	        					<button className="btn btn-default">Cancel</button>
	        					<button className="btn btn-success">Download(3)</button>
        					</div>
		        		</div>
		        	</div>
		      </div>);
    }
}

export default DownloadReport;
