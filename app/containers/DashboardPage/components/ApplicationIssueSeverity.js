import React from 'react';
import PropTypes from 'prop-types';
// Sy - Sub-Components

class ApplicationIssueSeverity extends React.Component {
    render() {
    	console.log("Severity", this.props.severity);
        return (<div className="col">
		        <div className="issue-item">
		          <div className={"number " + this.props.severity.className}>{this.props.severity.total}</div>
		          <div className="desc">{this.props.severity.name}</div>
		        </div>
		      </div>);
    }
}

/*ApplicationIssueSeverity.propTypes = {
	severity: PropTypes.object.isRequired
};*/

export default ApplicationIssueSeverity;
