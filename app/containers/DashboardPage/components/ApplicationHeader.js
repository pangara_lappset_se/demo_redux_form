import React from 'react';
import PropTypes from 'prop-types';
// Sy - Sub-Components

class ApplicationHeader extends React.Component {
	
    render() {
        return (<h3 className="application-title">Application A
                	<button type="button" className="btn btn-sm btn-primary float-right" data-toggle="modal" data-target="#downloadReportModal">
        				Download Reports&nbsp;&nbsp;
        				<i className="fas fa-caret-down"></i>
            		</button>
                	<button type="button" className="btn btn-sm btn-default float-right mr-md-3">Settings</button>
                	<p className="h6 pt-2">ID: hie72894hnf2</p>
                </h3>);
    }
}

export default ApplicationHeader;
