import React from 'react';
import PropTypes from 'prop-types';
// Sy - Sub-Components

var PieChart = require("react-chartjs-2").Pie;

class ApplicationIssueTool extends React.Component {

	 render() {
		const pieChart = this.props.pieChart;

		console.log('CHART DATA', pieChart);

	    return <PieChart data={pieChart.data} options={pieChart.options} width="600" height="250"/>
	 }
}
export default ApplicationIssueTool;
