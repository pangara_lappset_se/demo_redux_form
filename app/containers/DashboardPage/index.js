/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */
import React from 'react';
import { Helmet } from 'react-helmet';

// Sy - Sub-Components
import { Link } from 'react-router-dom';
import LeftSideBar from 'components/LeftSideBar';
import Breadcrumb from 'components/Breadcrumb';
import ApplicationHeader from './components/ApplicationHeader';
import ApplicationScore from './components/ApplicationScore';
import ApplicationScoreComplete from './components/ApplicationScoreComplete';
import ApplicationIssueSeverity from './components/ApplicationIssueSeverity';
import ApplicationIssueCategory from './components/ApplicationIssueCategory';
import ApplicationIssueTool from './components/ApplicationIssueTool';
import DownloadReport from './components/DownloadReport';

export class DashboardPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

	constructor (props) {
		super(props);
		
		// // Score data
	    let scoreDataObj = {
			  coverity  : {score: 90, last_run: '10:30 2017/10/27'},
			  protocode : {score: 40, last_run: '10:31 2017/10/27'},
			  ms_portal : {score: 20, last_run: '10:32 2017/10/27'},
			  score_complete : {pass: 3, total: 3}
	    };
	    
	    let pieChartData = {
			 datasets: [{
				 data:[4, 5, 7],
				 backgroundColor: ['#88B428', '#F29200', '#4886B3']
			 }],
			labels: ['Coveriry', 'Protocode', 'Ms Portal']
		}
		
		let pieChartConfig = {
            data: pieChartData,
            options: {
                responsive: true,
                animation: {
                	animateScale: true
                }
            }
        }
	    
	    let stackBarChartData = {
    		 labels: [
    			 'Injection',
    			 'Broken Authentication and Session Management',
    			 'Cross-Site Scripting (XSS)', 'Broken Access Control',
    			 'Security Misconfiguration',
    			 'Security Data Exposure',
    			 'Insufficient Attack Protection',
    			 'Cross-Site Request Forgery (CSRF)',
    			 'Using Components with Know Vulnerabilities',
    			 'Underprotected APIs'
			 ],
			 datasets: [
				 {
					 label: 'Coveriry',
					 data:[1, 3, 4, 7, 6, 2, 8, 2, 1, 4],
					 backgroundColor: '#88B428'
				 },
				 {
					 label: 'Protocode',
					 data:[4, 5, 2, 8, 10, 2, 4, 2, 1, 3],
					 backgroundColor: '#F29200'
				 },
				 {
					 label: 'Ms Portal',
					 data:[2, 7, 4, 4, 8, 6, 2 , 2 , 4, 1],
					 backgroundColor: '#4886B3'
				 }
			 ],
			
		}
	    
	    let stackBarChartConfig = {
	            data: stackBarChartData,
	            options: {
	            	barThickness: 0.4,
	            	tooltips: {
	            		mode: 'index',
	            		intersect: false
	            	},
	            	scales: {
	            		xAxes: [{
	            			stacked: true,
	            			ticks: {
	            				beginAtZero: true,
	            				/*steps: 1,
	            				stepValue: 10,
	            				max: 100*/
	            			}
	            		}],
	            		yAxes: [{
	            			stacked: true
	            		}]
	            	},
	                responsive: true,
	                animation: {
	                	animateScale: true
	                }
	            }
	        }
		
        this.state = {
            scoreData : scoreDataObj,
            pieChart  : pieChartConfig,
            stackBarChart  : stackBarChartConfig
        }  
    }
    
    componentDidMount() {
        console.log('state', this.state);
    }
	
	// Render Issue Severity
	renderSeverity(severityData) {
        if (severityData.length > 0) {
            return severityData.map((severity) => (<ApplicationIssueSeverity key={severity.id} severity={severity}/>));
        }
        return [];
    }
	
	// Render page
	render() {
		
	  const menu = [
          {
              id: 1,
              name: 'ALL APPS'
          },
          {
              id: 2,
              name: 'APPLICATION A'
          }
      ];
	  
	  // Severity data
    const severityData = [
    	{
    		id: 1,
    		name: 'Vulnerabilites',
    		total: 205,
    		className: 'red-text'
    	}, 
    	{
    		id: 2,
    		name: 'Critical',
    		total: 104,
    		className: 'yellow-text'
    	}, 
    	{
    		id: 3,
    		name: 'High',
    		total: 93,
    		className: 'yellow-text'
    	},
    	{
    		id: 4,
    		name: 'Medium',
    		total: 3,
    		className: 'blue-text'
    	}, {
    		id: 5,
    		name: 'Low',
    		total: 3,
    		className: 'green-text'
    	}, 
    	{
    		id: 6,
    		name: 'Information',
    		total: 3,
    		className: 'blue-text'
    	}
    ];
	  
	const severity 			= this.renderSeverity(severityData);
    const {scoreData} 		= this.state;
    const {pieChart} 		= this.state;
    const {stackBarChart} 	= this.state;
    
    return (
      <section className="main-wrapper">
        <DownloadReport />
        <Helmet>
          <title>Dashboard Page</title>
          <meta name="description" content="This is dashboard page" />
        </Helmet>
        <div>
          <LeftSideBar />
          <div className="content-wrapper">
          <Breadcrumb menu={menu}></Breadcrumb>
          <div className="main-content">
              <div className="page application-page">
              	<ApplicationHeader />
                  
                <section className="section score-section">
                	<div className="title">Score</div>
                	<ApplicationScore scoreData={scoreData} />
        			<ApplicationScoreComplete scoreData={scoreData} />
                </section>
                  
            	<section className="section issue-severity-section">
            		<ul className="list-inline text-center">
            			<li className="list-inline-item active"><a className="btn btn-default" href="#">All</a></li>
            			<li className="list-inline-item"><a className="btn btn-default" href="#">Coverity</a></li>
            			<li className="list-inline-item"><a className="btn btn-default" href="#">Protocode</a></li>
            			<li className="list-inline-item"><a className="btn btn-default" href="#">MS-Portal</a></li>
            		</ul>
                  	<div className="title">Issues by severities</div>
                  	<div className="row">
                  		{severity}
                  	</div>
                </section>
                  
                <section className="section issue-category-section">
                  	<div className="title">Issues by categories (OWASP Top 10)</div>
                	<ApplicationIssueCategory stackBarChart={stackBarChart} />
                </section>
                  
                <section className="section issue-tool-section">
                  	<div className="title">Issues by tool</div>
	              	<ApplicationIssueTool pieChart={pieChart} />
                </section>
              </div>
          </div>
      </div>


        </div> {/* Sy - End: Page main content */}
      </section>
    );
  }
}

export default DashboardPage;
