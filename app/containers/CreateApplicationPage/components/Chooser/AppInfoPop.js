import React from 'react';

const subOpts = [
    {
        name: 'Instance',
        val: '1'
    }, 
    {
        name: 'Project',
        val: '2'
    },
    {
        name: 'Stream',
        val: '3'
    }

]

class AppInfoPop extends React.Component {
    state = {
        mode: null,
        subMode:null,
        subOpt: []
    }

    // Event Click
    clickOption (e, opt) {
        e.preventDefault()
        this.setState({
            mode: opt
        });
        return false
    }

    clickChoose (e) {
        e.preventDefault();
        console.log('Choose this one');
        return false;
    }

    render() {
        const {mode} = this.state
        return (
            <div className="app-info-blk">
                <div className="app-info-ct">
                    <div className="app-src-blk">
                        <div className="row">
                            <div className="col-2">
                                <label className="lbl">Tools</label>
                            </div>
                            <div className="col-10">
                                <div className="row">
                                    <div className="col-4">
                                        <label
                                            onClick={(e) => this.clickOption(e, 'CIM')} 
                                            className={"src-opt" + ((mode == 'CIM') ? ' active' : '')}>CIM</label>
                                    </div>
                                    <div className="col-4">
                                        <label
                                            onClick={(e) => this.clickOption(e, 'PRO')}  
                                            className={"src-opt" + ((mode == 'PRO') ? ' active' : '')}>Protocode</label>
                                    </div>
                                    <div className="col-4">
                                        <label
                                             onClick={(e) => this.clickOption(e, 'MSP')}   
                                            className={"src-opt" + ((mode == 'MSP') ? ' active' : '')}>MS Protal</label>
                                    </div>
                                </div>
                                <div className="row">
                                    {(mode == 'CIM' || mode == 'PRO') &&
                                        <div className="col-4">
                                            <label className="src-opt">Instance</label>
                                        </div>
                                    }
                                    {(mode == 'CIM' || mode == 'PRO') &&
                                    <div className="col-4">
                                        <label className="src-opt">Project</label>
                                    </div>
                                    }
                                    {(mode == 'CIM') &&
                                    <div className="col-4">
                                        <label className="src-opt">Stream</label>
                                    </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* END: Tools */}
                    <div className="record-tbl">
                        <table className="tbl">
                        <thead>
                            <tr>
                                <th colSpan="2">
                                    Project Name
                                </th>
                                <th className="w20">
                                    Tool
                                </th>
                                <th className="w20">
                                    Tool Instance
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td className="w10">
                                    <span className="uncheck-box" onClick={(e) => this.clickChoose(e)}>
                                    &nbsp;
                                    </span>
                                </td>
                                <td>
                                    Project Name
                                </td>
                                <td>
                                    Tool
                                </td>
                                <td>
                                    Tool Instance
                                </td>
                            </tr>
                            
                            <tr>
                                <td className="w10">
                                    <i 
                                        onClick={(e) => this.clickChoose(e)}
                                        className="far fa-check-circle"></i>
                                </td>
                                <td>
                                    Project Name
                                </td>
                                <td>
                                    Tool
                                </td>
                                <td>
                                    Tool Instance
                                </td>
                            </tr>
                        </tbody>
                        </table>
                    </div>
                    {/* END: TABLE RECORD */}
                </div>
                <div className="app-info-footer clearfix">
                    <a href="#" className="btn blue-btn s-sm float-right">
                        Add (12)
                    </a>
                    <a href="#" className="btn s-sm float-right">
                        Clear
                    </a>
                </div>
            </div>
        )
    } 
}

export default AppInfoPop;